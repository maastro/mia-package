# MIA Package #

This package contains all microservices and dependencies for the Medical Image Analysis (MIA) framework. 


### MIA microservices

- **containerservice:** management of MIA containers
- **elasticsearchservice:** eureka service registry
- **discoveryserver:** eureka service registry
- **fileservice:** indexing of DICOM files
- **gui:** user interface
- **mappingservice:** maps RTOGs to ROIs
- **resultservice:** storage of computation results
- **schedulingservice:** task scheduler for MIA workers 
- **universalconfigurationservice:** configuration of MIA containers
- **universalmanager:** manages the overall workflow
- **universalworker:** processing of MIA containers

 
### Requirements

- 8 GB RAM
- Modern browser


### Getting started

1. Install services
  	- run ./bin/install.bat as administrator
2. Start services
	- run ./bin/start.bat as administrator
3. Wait until the [service registry](http://localhost:8761) (opened by start.bat) opens in your webbrowser and verify that all **10** MIA microservices are up and running (refreshing may be necessary). 
4. Go to the [user interface](http://localhost:8080) and login with username 'admin' and password 'admin'
5. Optional - Startup type auto for installed MIA services: 
	- ./bin/startup-type-auto.bat (run MIA at startup)
5. Optional - Uninstall:
	- ./bin/uninstall.bat 


#### Troubleshooting

 - Calculation Error

   - Solution: check logfile of the universal worker
   
        .\miapackage\services\universalworker\universalworker.log

- Error: Unrecognized character escape 'U' (code 85)
	
		com.fasterxml.jackson.databind.JsonMappingException: Unrecognized character escape 'U' (code 85)
		
	- Solution: Make sure forward slashes are escaped, or use backward slashes

- Error: Python not recognized 
 
	- Solution: The "universal worker" should "run as user/account" which has Python added to its path. If the worker runs as a windows service configure the correct user under: services, service properties, log on, This account.


### Further documentation
Please refer to the [general documentation](https://bitbucket.org/maastrosdt/mia/src/master/src/main/resources/README/README.md?fileviewer=file-view-default) for more details.

