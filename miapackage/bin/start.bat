﻿@echo off

echo Starting MIA Services...

echo Starting ElasticSearch ...
"%~dp0..\services\elasticsearchservice\elasticsearch.exe" start

echo Starting DiscoveryService (Eureka) ...
"%~dp0..\services\discoveryserver\discoveryserver.exe" start

echo Starting UserInterface ...
"%~dp0..\services\gui\gui.exe" start

echo Starting ContainerService ...
"%~dp0..\services\containerservice\containerservice.exe" start

echo Starting MappingService ...
"%~dp0..\services\mappingservice\mappingservice.exe" start

echo Starting ResultService ...
"%~dp0..\services\resultservice\resultservice.exe" start

echo Starting SchedulingService ...
"%~dp0..\services\schedulingservice\schedulingservice.exe" start

echo Starting UniversalConfigurationService
"%~dp0..\services\universalconfigurationservice\universalconfigurationservice.exe" start

echo Starting UniversalManager ...
"%~dp0..\services\universalmanager\universalmanager.exe" start

echo Starting UniversalWorker ...
"%~dp0..\services\universalworker\universalworker.exe" start

echo.
CALL :waitForPort ElasticSearch , 9200

echo Starting FileService ...
"%~dp0..\services\fileservice\fileservice.exe" start

echo.
CALL :waitForPort DiscoveryService , 8761

echo.
CALL :waitForPort UserInterface , 8080

pause
start "discoveryserver" http://localhost:8761
start "gui" http://localhost:8080
EXIT /B %ERRORLEVEL%


:waitForPort
@echo off
echo. 
echo|set /p="Waiting for %~1 to register on port %~2 ..."
:checkPort
timeout 1 > nul
netstat -a -n -o | findstr :%2| findstr "LISTENING ABHÖREN" > nul
IF ERRORLEVEL 1 (
 echo|set /p="." 
 goto checkPort
)
echo  OK
EXIT /B 0
