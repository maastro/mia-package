@echo off
"%~dp0..\services\elasticsearchservice\elasticsearch.exe" uninstall
"%~dp0..\services\containerservice\containerservice.exe" uninstall
"%~dp0..\services\discoveryserver\discoveryserver.exe" uninstall
"%~dp0..\services\fileservice\fileservice.exe" uninstall
"%~dp0..\services\gui\gui.exe" uninstall
"%~dp0..\services\mappingservice\mappingservice.exe" uninstall
"%~dp0..\services\resultservice\resultservice.exe" uninstall
"%~dp0..\services\schedulingservice\schedulingservice.exe" uninstall
"%~dp0..\services\universalconfigurationservice\universalconfigurationservice.exe" uninstall
"%~dp0..\services\universalmanager\universalmanager.exe" uninstall
"%~dp0..\services\universalworker\universalworker.exe" uninstall

echo.
pause
