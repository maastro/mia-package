@echo off
"%~dp0..\services\elasticsearchservice\elasticsearch.exe" stop
"%~dp0..\services\containerservice\containerservice.exe" stop
"%~dp0..\services\discoveryserver\discoveryserver.exe" stop
"%~dp0..\services\fileservice\fileservice.exe" stop
"%~dp0..\services\gui\gui.exe" stop
"%~dp0..\services\mappingservice\mappingservice.exe" stop
"%~dp0..\services\resultservice\resultservice.exe" stop
"%~dp0..\services\schedulingservice\schedulingservice.exe" stop
"%~dp0..\services\universalconfigurationservice\universalconfigurationservice.exe" stop
"%~dp0..\services\universalmanager\universalmanager.exe" stop
"%~dp0..\services\universalworker\universalworker.exe" stop

echo.
pause
