@echo off
"%~dp0..\services\elasticsearchservice\elasticsearch.exe" install
"%~dp0..\services\containerservice\containerservice.exe" install
"%~dp0..\services\discoveryserver\discoveryserver.exe" install
"%~dp0..\services\fileservice\fileservice.exe" install
"%~dp0..\services\gui\gui.exe" install
"%~dp0..\services\mappingservice\mappingservice.exe" install
"%~dp0..\services\resultservice\resultservice.exe" install
"%~dp0..\services\schedulingservice\schedulingservice.exe" install
"%~dp0..\services\universalconfigurationservice\universalconfigurationservice.exe" install
"%~dp0..\services\universalmanager\universalmanager.exe" install
"%~dp0..\services\universalworker\universalworker.exe" install

echo.
pause
