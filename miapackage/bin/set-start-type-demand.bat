sc config mia-elasticsearch start= demand
sc config mia-containerservice start= demand
sc config mia-discoveryserver start= demand
sc config mia-fileservice start= demand
sc config mia-gui start= demand
sc config mia-mappingservice start= demand
sc config mia-resultservice start= demand
sc config mia-schedulingservice start= demand
sc config mia-configurationservice start= demand
sc config mia-manager start= demand
sc config mia-universalworker-01 start= demand

echo.
pause
