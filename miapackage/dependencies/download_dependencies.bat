@echo off
set PATH=%PATH%;"%~dp0\GnuWin32\bin";"%~dp0\7z1604-extra"
set HOME=%~dp0

REM wget --no-check-certificate "https://downloads.sourceforge.net/project/sevenzip/7-Zip/16.04/7z1604-extra.7z?use_mirror=freefr"
REM 7za.exe x 7z1604-extra.7z

wget --no-check-certificate "https://nl.mathworks.com/supportfiles/downloads/R2015b/deployment_files/R2015b/installers/win64/MCR_R2015b_win64_installer.exe"
REM 7za.exe x MCR_R2015b_win64_installer.exe -o"%~dp0\matlab2015binstaller\"
REM cd %HOME%\matlab2015binstaller\bin\win64\
REM setup -mode silent -agreeToLicense yes -destinationFolder "C:\mia\miapackage\install\matlab2015binstaller\mcr"

wget --no-check-certificate "https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.4.0.zip"
REM 7za.exe x elasticsearch-5.4.0.zip -o"%~dp0..\dependencies\"

wget --no-check-certificate "https://downloads.sourceforge.net/portableapps/jPortable64_8_Update_131.paf.exe?"
REM 7za.exe x jPortable64_8_Update_131.paf.exe"%~dp0..\dependencies\"