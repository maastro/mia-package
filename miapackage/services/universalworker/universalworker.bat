@echo off

SET APP_NAME=universalworker

set HOME=%~dp0
set JAVA_PATH="%~dp0\..\..\dependencies\Java64\bin"
set MATLAB_PATH_REL=%~dp0\..\..\dependencies\MATLAB Runtime\v90\runtime\win64

rem add absolute matlab path to PATH
set MATLAB_PATH_ABS=
pushd %MATLAB_PATH_REL%
set MATLAB_PATH_ABS=%CD%
set PATH=%MATLAB_PATH_ABS%;%PATH%

TITLE %APP_NAME%

CD %HOME%
for %%f in (*.jar) do SET JAR_FILE=%%~nf.jar
"%JAVA_PATH%\java" -jar %JAR_FILE%