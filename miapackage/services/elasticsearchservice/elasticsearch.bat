@echo off
echo Starting ElasticSearch ...

SET JAVA_HOME="%~dp0..\..\dependencies\Java64"
call "%~dp0..\..\dependencies\elasticsearch-5.3.1\bin\elasticsearch.bat"