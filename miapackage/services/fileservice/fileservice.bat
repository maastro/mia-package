@echo off

SET APP_NAME=fileservice

SET JAVA_PATH="%~dp0\..\..\dependencies\Java64\bin"
SET PATH=%PATH%;%JAVA_PATH%

TITLE %APP_NAME%

for %%f in (*.jar) do set JAR_FILE=%%~nf.jar
"%JAVA_PATH%\java" -Xmx512m -jar %JAR_FILE%
