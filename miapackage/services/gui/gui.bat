@echo off

SET APP_NAME=gui

SET JAVA_PATH="%~dp0\..\..\dependencies\Java64\bin"
SET PATH=%PATH%;%JAVA_PATH%

TITLE %APP_NAME%

for %%f in (*.war) do set JAR_FILE=%%~nf.war
"%JAVA_PATH%\java" -Xmx512m -jar %JAR_FILE%
